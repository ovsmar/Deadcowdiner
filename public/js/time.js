let d = new Date();
let n = d.getDay();
let now = d.getHours() + "." + d.getMinutes();
let weekdays = [
        ["Sunday", 12.00, 14.00, 19.00,22.00],
        ["Monday"], // Restaurant fermé le lundi
        ["Tuesday", 12.00, 14.00, 19.00,22.00],
        ["Wednesday", 12.00, 14.00, 19.00,22.00],
        ["Thursday", 12.00, 14.00, 19.00,22.00],
        ["Friday", 12.00, 14.00, 19.00,22.00],
        ["Saturday", 12.00, 14.00, 19.00,22.00]
    ];
    let day = weekdays[n];

// Verifie la date et l'heure selon les horaires du restaurant
    if (now > day[1] && now < day[2] || now > day[3] && now < day[4]) {
  document.getElementById("currently_open").innerHTML=" Restaurant Ouvert";
  document.getElementById("open").classList.remove("close");
    }
     else {
  document.getElementById("currently_open").innerHTML=" Restaurant Fermé";
  document.getElementById("open").classList.add("close");
  document.getElementById("currently_open").classList.add("currently_closed");
    };

    
    
    const slider = document.querySelector('.slider');
let isDown = false;
let startX;
let sLeft;
slider.scrollLeft = 0;

slider.addEventListener('mousedown', (e) => {
  isDown = true;
  startX = e.pageX;
  sLeft = slider.scrollLeft;
});

slider.addEventListener('mouseleave', () => {
  isDown = false;
});
slider.addEventListener('mouseup', () => {
  isDown = false;
});

slider.addEventListener('mousemove', (e) => {
  if(!isDown) return;
  e.preventDefault();
  const x = e.pageX;
  const dragged = x - startX;
  slider.scrollLeft = sLeft - dragged;
}); 

