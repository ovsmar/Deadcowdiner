<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProduitsRepository;
use Symfony\Component\HttpFoundation\Request;

class CarteController extends AbstractController
{
    #[Route('/carte', name: 'app_carte',methods:['GET'])]
    public function index(ProduitsRepository $produitsRepository)
    
    {
        
        $contex= array('titre' => 'coucou' , 
        'produits'=> $produitsRepository->findAll(), 
        'show'=> false
    );

        return $this->render('cart.html.twig',$contex);
    }
     }
     