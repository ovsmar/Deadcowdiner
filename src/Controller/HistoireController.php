<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ImagesRepository;


class HistoireController extends AbstractController
{
    #[Route('/histoire', name: 'app_histoire', methods: ['GET'])]
    public function index(ImagesRepository $imagesRepository)
    {
        
        $contex= array('titre' => 'coucou' , 
        'images' => $imagesRepository->findAll(),
        'showEdit'=> false
    );

        return $this->render('histoire.html.twig',$contex);
    }
}