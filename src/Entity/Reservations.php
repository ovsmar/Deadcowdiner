<?php

namespace App\Entity;

use App\Repository\ReservationsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReservationsRepository::class)]
class Reservations
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $nom;

    #[ORM\Column(type: 'string', length: 50)]
    private $email;

    #[ORM\Column(type: 'integer')]
    private $telephone;

    #[ORM\Column(type: 'integer')]
    private $nombre_de_personnes;

    #[ORM\Column(type: 'time')]
    private $heures;

    #[ORM\Column(type: 'date')]
    private $date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTelephone(): ?int
    {
        return $this->telephone;
    }

    public function setTelephone(int $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getNombreDePersonnes(): ?int
    {
        return $this->nombre_de_personnes;
    }

    public function setNombreDePersonnes(int $nombre_de_personnes): self
    {
        $this->nombre_de_personnes = $nombre_de_personnes;

        return $this;
    }

    public function getHeures(): ?\DateTimeInterface
    {
        return $this->heures;
    }

    public function setHeures(\DateTimeInterface $heures): self
    {
        $this->heures = $heures;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
