<?php

namespace App\Form;

use App\Entity\Reservations;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TimeType ;
use Symfony\Component\Form\Extension\Core\Type\DateType ;
use Symfony\Component\Form\Extension\Core\Type\EmailType ;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Validator\Constraints as Assert;


class ReservationsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom' ,TextType::class,[
                
                'attr' => [
                    'class' => 'form-control',
                    'min' => '2',
                    'max' => '100'
                ],
                'label' => 'Nom',
                'label_attr' => [
                    'class' => 'form-label mt-4'
                ],
                'constraints' => [
                    new Assert\Length(['min' => 2, 'max' => 100]),
                    new Assert\NotBlank()
                ]
            ])
            
            ->add('email',EmailType::class,[
                'attr' => [
                    'class' => 'form-control',
                    'min' => '2',
                    'max' => '100'
                ],
                'label' => 'Adresse e-mail',
                'label_attr' => [
                    'class' => 'form-label mt-4'
                ],
                'constraints' => [
                    new Assert\Length(['min' => 2, 'max' => 100]),
                    new Assert\NotBlank()
                ]  
            ])
            
            ->add('telephone', TelType::class,[
                
            
                'attr' => [
                    'class' => 'form-control',
                    'min' => '10',
                    'max' => '10'
                ],
                'label' => 'Téléphone',
                'label_attr' => [
                    'class' => 'form-label mt-4'
                ],
                'constraints' => [
                    new Assert\Length(['min' => 10, 'max' => 10]),
                    new Assert\NotBlank(),
                    new Assert\Positive()
                ]
            
            ])

            
            ->add('nombre_de_personnes',IntegerType::class,[
                
                'attr' => [
                    'class' => 'form-control',
                    'min' => '1',
                    'max' => '100'
                ],
                'label' => 'Nombre de couverts',
                'label_attr' => [
                    'class' => 'form-label mt-4'
                ],
                'constraints' => [
                    new Assert\Length(['min' => 1, 'max' => 100]),
                    new Assert\NotBlank(),
                    new Assert\Positive()
                ]
            ])


            
            ->add('heures',TimeType ::class,[
                'hours' => [12,13,14,19, 20, 21, 22,] 
            ] )
            
            ->add('date' , DateType ::class,[
                'years' => [2022,2023] ,
                'placeholder' =>[
                    'year' => 'Year', 'month' => 'Month', 'day' => 'Day',
                    'hour' => 'Hour', 'minute' => 'Minute', 'second' => 'Second',
                ]
             ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reservations::class,
        ]);
    }
}