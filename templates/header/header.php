{% block stylesheets %}
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Amatic+SC" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="/index.css" rel="stylesheet">
{% endblock %}
<header>

    <!-- <div class="wrapper">
        <a href="/index">
            <img class="logo" src="/images/logo_head.png" alt="logo" width="250px" center cover />
        </a>
    </div>
    <div class="wrapper" id="title">
        <h1>Dead Cow Diner</h1>
    </div> -->
    <a class="icon" onclick="myFunction()">&#9776;</a>
</header>
<nav id="navbar" class="nav">
    <ul>
        <div id="idlogo">
            <a href="/">
                <img class="logo" src="/images/logo_head.png" alt="logo" width="250px" center cover />
            </a>
        </div>
        <li><a href="/histoire">Notre histoire</a></li>
        <li><a href="/carte">Carte</a></li>
        <li><a href="/coordonees">Coordonées</a></li>
        <li><a href="/avis">Votre avis</a></li>
    </ul>
</nav>